package org.bitbucket.yanrym.moneytransfer.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.javalin.Javalin;
import io.javalin.http.ExceptionHandler;
import io.javalin.http.Handler;
import org.bitbucket.yanrym.moneytransfer.dto.AccountCreateRequest;
import org.bitbucket.yanrym.moneytransfer.dto.ErrorResponse;
import org.bitbucket.yanrym.moneytransfer.dto.MoneyTransferRequest;
import org.bitbucket.yanrym.moneytransfer.exceptions.AccountNotFound;
import org.bitbucket.yanrym.moneytransfer.exceptions.ApiException;
import org.bitbucket.yanrym.moneytransfer.service.AccountService;
import org.bitbucket.yanrym.moneytransfer.service.MoneyService;

public class ApiServer {

    private Javalin service;
    private AccountService accountService;
    private MoneyService moneyService;

    private ObjectMapper mapper = new ObjectMapper();

    public ApiServer(Javalin service, AccountService accountService, MoneyService moneyService) {
        this.service = service;
        this.accountService = accountService;
        this.moneyService = moneyService;
    }

    public void create() {
        service.get("/accounts/:id", getAccount());
        service.get("/accounts/:id/balance", getAccountBalance());
        service.put("/accounts", createAccount());
        service.put("/accounts/:id/moneytransfers", transferMoney());

        service.exception(RuntimeException.class, handleRuntimeExceptions());
    }

    private ExceptionHandler<RuntimeException> handleRuntimeExceptions() {
        return (e, ctx) -> {
            if (e instanceof ApiException) {
                ctx.status(422);
                ctx.result(safeObjToString(new ErrorResponse(((ApiException) e).errorCode(), e.getMessage())));
            } else {
                ctx.status(500);
            }
        };
    }

    private Handler transferMoney() {
        return (ctx) -> {
            var accountFrom = Long.parseLong(ctx.pathParam("id"));
            var tr = mapper.readValue(ctx.body(), MoneyTransferRequest.class);
            moneyService.transferMoney(accountFrom, tr.accountTo, tr.amount);
        };
    }

    private Handler getAccountBalance() {
        return (ctx) -> {
            var accountId = Long.parseLong(ctx.pathParam("id"));
            moneyService
                    .getBalance(accountId)
                    .map((balance) -> ctx.result(safeObjToString(balance)))
                    .orElseThrow(() -> new AccountNotFound(accountId));
        };
    }

    private Handler getAccount() {
        return (ctx) -> {
            var accountId = Long.parseLong(ctx.pathParam("id"));
            accountService
                    .getAccount(accountId)
                    .map((account) -> ctx.result(safeObjToString(account)))
                    .orElseThrow(() -> new AccountNotFound(accountId));
        };
    }

    private Handler createAccount() {
        return (ctx) -> {
            var body = mapper.readValue(ctx.body(), AccountCreateRequest.class);
            var account = accountService.createAccount(body);
            ctx.result(mapper.writeValueAsString(account));
        };
    }

    private String safeObjToString(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Failed to serialize object " + obj, e);
        }
    }

}
