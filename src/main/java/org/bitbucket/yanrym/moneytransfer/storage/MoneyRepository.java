package org.bitbucket.yanrym.moneytransfer.storage;

import org.bitbucket.yanrym.moneytransfer.dto.MoneyTransfer;
import org.bitbucket.yanrym.moneytransfer.dto.MoneyTransferId;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class MoneyRepository {

    private final AtomicLong transferIdSeq = new AtomicLong();
    private final Set<MoneyTransferId> requestRegistry = new HashSet<>();
    private final Map<Long, List<MoneyTransfer>> accountTransfers = new HashMap<>();

    public synchronized List<MoneyTransfer> getTransfers(long accountId) {
        return accountTransfers.getOrDefault(accountId, Collections.emptyList());
    }

    public synchronized boolean saveTransfer(
            MoneyTransferId transferId,
            long accountTo,
            BigDecimal sourceAmount,
            BigDecimal targetAmount
    ) {
        if (requestRegistry.contains(transferId)) {
            return false;
        } else {
            var newId = transferIdSeq.getAndIncrement();
            var transfer = new MoneyTransfer(
                    newId, transferId.accountFrom, accountTo, sourceAmount, targetAmount
            );
            addTransferToAccount(transfer.from, transfer);
            addTransferToAccount(transfer.to, transfer);
            requestRegistry.add(transferId);
            return true;
        }
    }

    private void addTransferToAccount(long accountId, MoneyTransfer transfer) {
        List<MoneyTransfer> transferList = accountTransfers.get(accountId);
        List<MoneyTransfer> newList;
        if (transferList != null) {
            newList = new ArrayList<>(transferList);
        } else {
            newList = new ArrayList<>();
        }
        newList.add(transfer);
        accountTransfers.put(accountId, newList);
    }
}
