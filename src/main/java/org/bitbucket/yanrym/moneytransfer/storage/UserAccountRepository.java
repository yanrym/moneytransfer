package org.bitbucket.yanrym.moneytransfer.storage;

import org.bitbucket.yanrym.moneytransfer.dto.Account;
import org.bitbucket.yanrym.moneytransfer.dto.AccountCreateRequest;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class UserAccountRepository {

    private AtomicLong accountIdSeq = new AtomicLong();
    private Map<Long, Account> accounts = new ConcurrentHashMap<>();

    public Account create(AccountCreateRequest request) {
        long id = accountIdSeq.getAndIncrement();
        var account = new Account();
        account.id = id;
        account.currencyCode = request.currencyCode;
        accounts.put(id, account);
        return account;
    }

    public Optional<Account> get(long id) {
        return Optional.ofNullable(accounts.get(id));
    }

}
