package org.bitbucket.yanrym.moneytransfer.dto;

import java.math.BigDecimal;

public class Balance {
    public BigDecimal balance;
    public CurrencyCode currencyCode;

    public Balance(BigDecimal balance, CurrencyCode currencyCode) {
        this.balance = balance;
        this.currencyCode = currencyCode;
    }

    public Balance() {
    }
}
