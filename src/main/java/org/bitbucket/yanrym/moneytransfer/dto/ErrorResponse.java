package org.bitbucket.yanrym.moneytransfer.dto;

public class ErrorResponse {
    public int errorCode;
    public String message;

    public ErrorResponse() {
    }

    public ErrorResponse(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }
}
