package org.bitbucket.yanrym.moneytransfer.dto;

public enum CurrencyCode {
    EUR, USD;
}
