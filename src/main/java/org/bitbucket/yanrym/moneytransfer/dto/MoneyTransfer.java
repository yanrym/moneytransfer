package org.bitbucket.yanrym.moneytransfer.dto;

import java.math.BigDecimal;

public class MoneyTransfer {
    public final long id;
    public final long from;
    public final long to;

    public final BigDecimal sourceAmount;
    public final BigDecimal targetAmount;

    public MoneyTransfer(long id, long from, long to, BigDecimal sourceAmount, BigDecimal targetAmount) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.sourceAmount = sourceAmount;
        this.targetAmount = targetAmount;
    }

}
