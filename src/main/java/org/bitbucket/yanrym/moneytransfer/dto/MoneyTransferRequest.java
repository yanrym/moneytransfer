package org.bitbucket.yanrym.moneytransfer.dto;

import java.math.BigDecimal;

public class MoneyTransferRequest {
    public long accountTo;
    public BigDecimal amount;

    public MoneyTransferRequest(long accountTo, BigDecimal amount) {
        this.accountTo = accountTo;
        this.amount = amount;
    }

    public MoneyTransferRequest() {
    }
}
