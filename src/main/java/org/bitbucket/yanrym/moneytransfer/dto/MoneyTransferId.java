package org.bitbucket.yanrym.moneytransfer.dto;

import java.util.Objects;

public class MoneyTransferId {

    public final long accountFrom;
    public final Long previousMoneyTransfer;

    public MoneyTransferId(long accountFrom, Long previousMoneyTransfer) {
        this.accountFrom = accountFrom;
        this.previousMoneyTransfer = previousMoneyTransfer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MoneyTransferId that = (MoneyTransferId) o;
        return accountFrom == that.accountFrom &&
                Objects.equals(previousMoneyTransfer, that.previousMoneyTransfer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountFrom, previousMoneyTransfer);
    }
}
