package org.bitbucket.yanrym.moneytransfer.dto;

public class AccountCreateRequest {

    public CurrencyCode currencyCode;

    public AccountCreateRequest(CurrencyCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    public AccountCreateRequest() {
    }
}
