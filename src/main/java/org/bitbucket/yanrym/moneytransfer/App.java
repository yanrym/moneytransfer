package org.bitbucket.yanrym.moneytransfer;

import io.javalin.Javalin;
import org.bitbucket.yanrym.moneytransfer.api.ApiServer;
import org.bitbucket.yanrym.moneytransfer.service.AccountService;
import org.bitbucket.yanrym.moneytransfer.service.MoneyService;
import org.bitbucket.yanrym.moneytransfer.service.NoOpCurrencyConversionService;
import org.bitbucket.yanrym.moneytransfer.storage.MoneyRepository;
import org.bitbucket.yanrym.moneytransfer.storage.UserAccountRepository;

public class App {

    public static void main(String[] args) {
        var app = Javalin.create().start(7001);

        var accountService = new AccountService(new UserAccountRepository());
        new ApiServer(
                app,
                accountService,
                new MoneyService(accountService, new MoneyRepository(), new NoOpCurrencyConversionService())
        ).create();
    }


}
