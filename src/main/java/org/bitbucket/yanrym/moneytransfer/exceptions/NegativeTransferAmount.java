package org.bitbucket.yanrym.moneytransfer.exceptions;

import java.math.BigDecimal;

public class NegativeTransferAmount extends RuntimeException implements ApiException {

    public static int ERROR_CODE = 2;

    private BigDecimal amount;

    public NegativeTransferAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String getMessage() {
        return "Transfer amount must be non-negative, got " + amount;
    }

    @Override
    public int errorCode() {
        return ERROR_CODE;
    }
}
