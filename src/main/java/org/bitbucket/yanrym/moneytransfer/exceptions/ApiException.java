package org.bitbucket.yanrym.moneytransfer.exceptions;

public interface ApiException {
    int errorCode();
}
