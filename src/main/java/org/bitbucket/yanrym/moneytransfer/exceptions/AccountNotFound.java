package org.bitbucket.yanrym.moneytransfer.exceptions;

public class AccountNotFound extends RuntimeException implements ApiException {

    public static int ERROR_CODE = 1;

    public long id;

    public AccountNotFound(long id) {
        this.id = id;
    }

    @Override
    public String getMessage() {
        return "Could not find account with id " + id;
    }

    @Override
    public int errorCode() {
        return ERROR_CODE;
    }
}
