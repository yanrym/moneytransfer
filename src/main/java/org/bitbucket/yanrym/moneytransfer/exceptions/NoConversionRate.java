package org.bitbucket.yanrym.moneytransfer.exceptions;

import org.bitbucket.yanrym.moneytransfer.dto.CurrencyCode;

public class NoConversionRate extends RuntimeException implements ApiException {
    public static int ERROR_CODE = 4;

    private CurrencyCode from;
    private CurrencyCode to;

    public NoConversionRate(CurrencyCode from, CurrencyCode to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public String getMessage() {
        return "Can't find conversion rate from " + from + " to " + to;
    }

    @Override
    public int errorCode() {
        return ERROR_CODE;
    }
}
