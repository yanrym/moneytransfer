package org.bitbucket.yanrym.moneytransfer.exceptions;

import java.math.BigDecimal;

public class NotEnoughBalance extends RuntimeException implements ApiException {

    public static int ERROR_CODE = 3;
    private final BigDecimal have;
    private final BigDecimal requested;

    public NotEnoughBalance(BigDecimal have, BigDecimal requested) {
        this.have = have;
        this.requested = requested;
    }

    @Override
    public String getMessage() {
        return "Not enough balance to transfer money. Got " + have + ", requested " + requested;
    }

    @Override
    public int errorCode() {
        return ERROR_CODE;
    }
}
