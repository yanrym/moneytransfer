package org.bitbucket.yanrym.moneytransfer.service;

import org.bitbucket.yanrym.moneytransfer.dto.Account;
import org.bitbucket.yanrym.moneytransfer.dto.AccountCreateRequest;
import org.bitbucket.yanrym.moneytransfer.storage.UserAccountRepository;

import java.util.Optional;

public class AccountService {

    private UserAccountRepository userRepo;

    public AccountService(UserAccountRepository userRepo) {
        this.userRepo = userRepo;
    }

    public Account createAccount(AccountCreateRequest request) {
        return userRepo.create(request);
    }

    public Optional<Account> getAccount(long id) {
        return userRepo.get(id);
    }

}
