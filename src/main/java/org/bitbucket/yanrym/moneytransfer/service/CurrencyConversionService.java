package org.bitbucket.yanrym.moneytransfer.service;

import org.bitbucket.yanrym.moneytransfer.dto.CurrencyCode;

import java.math.BigDecimal;

public interface CurrencyConversionService {
    BigDecimal getConversionRate(CurrencyCode from, CurrencyCode to);
}
