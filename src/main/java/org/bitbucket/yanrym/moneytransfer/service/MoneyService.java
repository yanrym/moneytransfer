package org.bitbucket.yanrym.moneytransfer.service;

import org.bitbucket.yanrym.moneytransfer.dto.Account;
import org.bitbucket.yanrym.moneytransfer.dto.Balance;
import org.bitbucket.yanrym.moneytransfer.dto.MoneyTransferId;
import org.bitbucket.yanrym.moneytransfer.exceptions.AccountNotFound;
import org.bitbucket.yanrym.moneytransfer.exceptions.NegativeTransferAmount;
import org.bitbucket.yanrym.moneytransfer.exceptions.NotEnoughBalance;
import org.bitbucket.yanrym.moneytransfer.service.helpers.AccountPair;
import org.bitbucket.yanrym.moneytransfer.service.helpers.LatestBalance;
import org.bitbucket.yanrym.moneytransfer.storage.MoneyRepository;

import java.math.BigDecimal;
import java.util.Optional;

public class MoneyService {

    private AccountService accountService;
    private MoneyRepository moneyRepository;
    private CurrencyConversionService conversionService;

    public MoneyService(
            AccountService accountService,
            MoneyRepository moneyRepository,
            CurrencyConversionService conversionService
    ) {
        this.accountService = accountService;
        this.moneyRepository = moneyRepository;
        this.conversionService = conversionService;
    }

    public Optional<Balance> getBalance(long accountId) {
        var account = accountService.getAccount(accountId);
        return account.map((acc) -> {
            var latestAmount = getLatestBalance(acc.id).amount;
            return new Balance(latestAmount, acc.currencyCode);
        });
    }

    public void transferMoney(long accountFrom, long accountTo, BigDecimal amount) {
        if (amount == null || isNegative(amount)) {
            throw new NegativeTransferAmount(amount);
        }
        var accounts = prepareTransferAccounts(accountFrom, accountTo);

        boolean transferSuccess;
        do {
            transferSuccess = tryExecuteTransfer(accounts.from, accounts.to, amount);
        } while (!transferSuccess);
    }

    private AccountPair prepareTransferAccounts(long accountFrom, long accountTo) {
        Optional<Account> from = accountService.getAccount(accountFrom);
        if (from.isEmpty()) {
            throw new AccountNotFound(accountFrom);
        }
        Optional<Account> to = accountService.getAccount(accountTo);
        if (to.isEmpty()) {
            throw new AccountNotFound(accountTo);
        }
        return new AccountPair(from.get(), to.get());
    }

    private boolean tryExecuteTransfer(Account from, Account to, BigDecimal amount) {
        var balance = getLatestBalance(from.id);
        if (balance.amount.compareTo(amount) < 0) {
            throw new NotEnoughBalance(balance.amount, amount);
        }
        var conversionRate = conversionService.getConversionRate(
                from.currencyCode, to.currencyCode
        );
        var targetCurrencyAmount = amount.multiply(conversionRate);
        return moneyRepository.saveTransfer(
                new MoneyTransferId(from.id, balance.lastTransferId),
                to.id,
                amount,
                targetCurrencyAmount
        );
    }

    private LatestBalance getLatestBalance(long accountId) {
        return moneyRepository
                .getTransfers(accountId)
                .stream()
                .map((mt) -> {
                    BigDecimal amount;
                    if (mt.from == accountId) {
                        amount = mt.sourceAmount.negate();
                    } else {
                        amount = mt.targetAmount;
                    }
                    return new LatestBalance(amount, mt.id);
                })
                .reduce(new LatestBalance(), LatestBalance::merge);
    }

    private boolean isNegative(BigDecimal amount) {
        return amount.compareTo(BigDecimal.ZERO) < 0;
    }

}
