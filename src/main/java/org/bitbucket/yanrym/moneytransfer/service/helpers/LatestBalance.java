package org.bitbucket.yanrym.moneytransfer.service.helpers;

import java.math.BigDecimal;

public class LatestBalance {
    public final BigDecimal amount;
    public final Long lastTransferId;

    public LatestBalance(BigDecimal amount, Long lastTransferId) {
        this.amount = amount;
        this.lastTransferId = lastTransferId;
    }

    public LatestBalance() {
        this(BigDecimal.ZERO, Long.MIN_VALUE);
    }

    public static LatestBalance merge(LatestBalance left, LatestBalance right) {
        return new LatestBalance(
                left.amount.add(right.amount),
                Math.max(left.lastTransferId, right.lastTransferId)
        );
    }
}
