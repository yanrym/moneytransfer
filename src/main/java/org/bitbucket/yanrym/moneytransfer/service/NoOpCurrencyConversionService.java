package org.bitbucket.yanrym.moneytransfer.service;

import org.bitbucket.yanrym.moneytransfer.dto.CurrencyCode;

import java.math.BigDecimal;

public class NoOpCurrencyConversionService implements CurrencyConversionService {

    @Override
    public BigDecimal getConversionRate(CurrencyCode from, CurrencyCode to) {
        return BigDecimal.ONE;
    }
}
