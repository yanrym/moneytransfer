package org.bitbucket.yanrym.moneytransfer.service.helpers;

import org.bitbucket.yanrym.moneytransfer.dto.Account;

public class AccountPair {
    public Account from;
    public Account to;

    public AccountPair(Account from, Account to) {
        this.from = from;
        this.to = to;
    }
}
