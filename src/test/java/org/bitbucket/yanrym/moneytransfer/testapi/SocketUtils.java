package org.bitbucket.yanrym.moneytransfer.testapi;

import java.io.IOException;
import java.net.ServerSocket;

public class SocketUtils {
    public static int getPort() throws IOException {
        try (var socket = new ServerSocket(0)) {
            socket.setReuseAddress(true);
            return socket.getLocalPort();
        }
    }
}
