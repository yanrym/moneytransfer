package org.bitbucket.yanrym.moneytransfer.testapi;

import io.javalin.Javalin;
import org.bitbucket.yanrym.moneytransfer.api.ApiServer;
import org.bitbucket.yanrym.moneytransfer.dto.CurrencyCode;
import org.bitbucket.yanrym.moneytransfer.dto.MoneyTransferId;
import org.bitbucket.yanrym.moneytransfer.exceptions.NoConversionRate;
import org.bitbucket.yanrym.moneytransfer.service.AccountService;
import org.bitbucket.yanrym.moneytransfer.service.CurrencyConversionService;
import org.bitbucket.yanrym.moneytransfer.service.MoneyService;
import org.bitbucket.yanrym.moneytransfer.service.NoOpCurrencyConversionService;
import org.bitbucket.yanrym.moneytransfer.storage.MoneyRepository;
import org.bitbucket.yanrym.moneytransfer.storage.UserAccountRepository;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicLong;

public class TestApp {

    private static AtomicLong moneyTransferIdSeq = new AtomicLong();
    public static long NOT_EXISTING_ACCOUNT = Long.MIN_VALUE;

    public static ApiServer createBasicApp(Javalin service) {
        return createAppWithCtx(service).apiServer;
    }

    public static AppWithCtx createAppWithCtx(Javalin service) {
        var appWithCtx = new AppWithCtx();
        appWithCtx.moneyRepository = new TestMoneyRepo();
        var accountService = new AccountService(new UserAccountRepository());
        appWithCtx.apiServer = new ApiServer(
                service,
                accountService,
                new MoneyService(accountService, appWithCtx.moneyRepository, new NoOpCurrencyConversionService())
        );
        appWithCtx.apiServer.create();
        return appWithCtx;
    }

    public static AppWithConverter createAppWithEurToUsdConverter(Javalin service) {
        var appWithConv = new AppWithConverter();
        appWithConv.converter = new EurToUsdConverter();
        appWithConv.moneyRepository = new TestMoneyRepo();
        var accountService = new AccountService(new UserAccountRepository());
        appWithConv.apiServer = new ApiServer(
                service,
                accountService,
                new MoneyService(accountService, appWithConv.moneyRepository, appWithConv.converter)
        );
        appWithConv.apiServer.create();
        return appWithConv;
    }

    public static class AppWithCtx {
        public ApiServer apiServer;
        public TestMoneyRepo moneyRepository;
    }

    public static class AppWithConverter extends AppWithCtx {
        public EurToUsdConverter converter;
    }

    public static class TestMoneyRepo extends MoneyRepository {
        public void addMoneyToAccount(long accountId, BigDecimal amount) {
            var transferId = new MoneyTransferId(
                    NOT_EXISTING_ACCOUNT,
                    moneyTransferIdSeq.getAndIncrement()
            );
            var result = super.saveTransfer(transferId, accountId, amount, amount);
            Assertions.assertTrue(result, "Transfer wasn't saved");
        }
    }

    public static class EurToUsdConverter implements CurrencyConversionService {

        public BigDecimal rate = BigDecimal.ONE;

        @Override
        public BigDecimal getConversionRate(CurrencyCode from, CurrencyCode to) {
            if (from == CurrencyCode.EUR && to == CurrencyCode.USD) {
                return rate;
            }
            throw new NoConversionRate(from, to);
        }
    }

}
