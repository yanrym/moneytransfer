package org.bitbucket.yanrym.moneytransfer.testapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.javalin.Javalin;
import org.bitbucket.yanrym.moneytransfer.dto.*;
import org.bitbucket.yanrym.moneytransfer.exceptions.AccountNotFound;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;

public class ApiTestDriver {

    private Javalin service;

    public ApiTestDriver(Javalin service) {
        this.service = service;
    }


    static {
        Unirest.setObjectMapper(new com.mashape.unirest.http.ObjectMapper() {
            com.fasterxml.jackson.databind.ObjectMapper mapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public String writeValue(Object value) {
                try {
                    return mapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return mapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    private static ObjectMapper mapper = new ObjectMapper();

    public Optional<Account> getAccount(long id) throws UnirestException, IOException {
        var response = Unirest
                .get(baseUrl() + "/accounts/" + id)
                .asString();

        if (response.getStatus() == 422 && errorBody(response).errorCode == AccountNotFound.ERROR_CODE) {
            return Optional.empty();
        } else if (response.getStatus() == 200) {
            return Optional.of(mapper.readValue(response.getBody(), Account.class));
        } else {
            throw new RuntimeException(
                    "Could not retrieve account " + id + ". Error code : " + response.getStatusText()
            );
        }
    }

    public Optional<Balance> getBalance(long id) throws UnirestException, IOException {
        var response = Unirest
                .get(baseUrl() + "/accounts/" + id + "/balance")
                .asString();

        if (response.getStatus() == 422 && errorBody(response).errorCode == AccountNotFound.ERROR_CODE) {
            return Optional.empty();
        } else if (response.getStatus() == 200) {
            return Optional.of(mapper.readValue(response.getBody(), Balance.class));
        } else {
            throw new RuntimeException(
                    "Could not retrieve account " + id + ". Error code : " + response.getStatusText()
            );
        }
    }

    public Account createAccount() throws UnirestException {
        return createAccount(CurrencyCode.EUR);
    }

    public Account createAccount(CurrencyCode code) throws UnirestException {
        var response = Unirest
                .put(baseUrl() + "/accounts")
                .body(new AccountCreateRequest(code))
                .asObject(Account.class);

        if (response.getStatus() == 200) {
            return response.getBody();
        } else {
            throw new RuntimeException(
                    "Could not create account. Error code : " + response.getStatusText()
            );
        }
    }

    public int transferMoney(long from, long to, BigDecimal amount) {
        try {
            var response = Unirest
                    .put(baseUrl() + "/accounts/" + from + "/moneytransfers")
                    .body(new MoneyTransferRequest(to, amount))
                    .asString();

            if (response.getStatus() == 422) {
                return errorBody(response).errorCode;
            } else {
                return -1;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String baseUrl() {
        return "http://localhost:" + service.port();
    }

    private ErrorResponse errorBody(HttpResponse<String> response) throws IOException {
        return mapper.readValue(response.getBody(), ErrorResponse.class);
    }

}
