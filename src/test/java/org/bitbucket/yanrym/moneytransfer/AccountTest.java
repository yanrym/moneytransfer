package org.bitbucket.yanrym.moneytransfer;

import com.mashape.unirest.http.exceptions.UnirestException;
import io.javalin.Javalin;
import org.bitbucket.yanrym.moneytransfer.testapi.ApiTestDriver;
import org.bitbucket.yanrym.moneytransfer.testapi.SocketUtils;
import org.bitbucket.yanrym.moneytransfer.testapi.TestApp;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

class AccountTest {

    private Javalin service;
    private ApiTestDriver driver;

    @BeforeEach
    void setUp() throws Exception {
        service = Javalin.create().start(SocketUtils.getPort());

        TestApp.createBasicApp(service);

        driver = new ApiTestDriver(service);
    }

    @AfterEach
    void tearDown() {
        service.stop();
    }

    @Test
    void cantFindNonExistingAccount() throws UnirestException, IOException {
        var actual = driver.getAccount(123);

        assertTrue(!actual.isPresent(), "Account doesn't exist");
    }

    @Test
    void retrievesCreatedAccount() throws UnirestException, IOException {
        var created = driver.createAccount();

        var actual = driver.getAccount(created.id);

        Assertions.assertEquals(created.id, actual.get().id);
    }

}
