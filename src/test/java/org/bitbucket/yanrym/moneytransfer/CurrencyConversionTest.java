package org.bitbucket.yanrym.moneytransfer;

import com.mashape.unirest.http.exceptions.UnirestException;
import io.javalin.Javalin;
import org.bitbucket.yanrym.moneytransfer.dto.CurrencyCode;
import org.bitbucket.yanrym.moneytransfer.testapi.ApiTestDriver;
import org.bitbucket.yanrym.moneytransfer.testapi.SocketUtils;
import org.bitbucket.yanrym.moneytransfer.testapi.TestApp;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CurrencyConversionTest {

    private Javalin service;
    private ApiTestDriver driver;
    private TestApp.AppWithConverter appWithConv;

    @BeforeEach
    void setUp() throws Exception {
        service = Javalin.create().start(SocketUtils.getPort());

        appWithConv = TestApp.createAppWithEurToUsdConverter(service);

        driver = new ApiTestDriver(service);
    }

    @AfterEach
    void tearDown() {
        service.stop();
    }

    @Test
    void moneyTransferAppliesCurrencyConversion() throws UnirestException, IOException {
        appWithConv.converter.rate = new BigDecimal("2");
        var accountFrom = driver.createAccount(CurrencyCode.EUR);
        var accountTo = driver.createAccount(CurrencyCode.USD);
        var originalAmount = new BigDecimal("10");
        var transferAmount = new BigDecimal("5");
        appWithConv.moneyRepository.addMoneyToAccount(accountFrom.id, originalAmount);

        driver.transferMoney(accountFrom.id, accountTo.id, transferAmount);
        var balanceFrom = driver.getBalance(accountFrom.id);
        var balanceTo = driver.getBalance(accountTo.id);

        assertEquals(new BigDecimal("5"), balanceFrom.get().balance);
        assertEquals(CurrencyCode.EUR, balanceFrom.get().currencyCode);
        assertEquals(new BigDecimal("10"), balanceTo.get().balance);
        assertEquals(CurrencyCode.USD, balanceTo.get().currencyCode);
    }

    @Test
    void throwsErrorWhenNoConversionRate() throws UnirestException, IOException {
        var accountFrom = driver.createAccount(CurrencyCode.USD);
        var accountTo = driver.createAccount(CurrencyCode.USD);
        var originalAmount = new BigDecimal("10");
        var transferAmount = new BigDecimal("5");
        appWithConv.moneyRepository.addMoneyToAccount(accountFrom.id, originalAmount);

        var errorCode = driver.transferMoney(accountFrom.id, accountTo.id, transferAmount);
        assertEquals(4, errorCode);

        var balanceFrom = driver.getBalance(accountFrom.id);
        var balanceTo = driver.getBalance(accountTo.id);

        assertEquals(originalAmount, balanceFrom.get().balance);
        assertEquals(BigDecimal.ZERO, balanceTo.get().balance);
    }

    @Test
    void changedRateDoesntApplyHistorically() throws UnirestException, IOException {
        var accountFrom = driver.createAccount(CurrencyCode.EUR);
        var accountTo = driver.createAccount(CurrencyCode.USD);
        var originalAmount = new BigDecimal("10");
        appWithConv.moneyRepository.addMoneyToAccount(accountFrom.id, originalAmount);

        appWithConv.converter.rate = new BigDecimal("2");
        driver.transferMoney(accountFrom.id, accountTo.id, new BigDecimal("5"));
        appWithConv.converter.rate = new BigDecimal("0.5");
        driver.transferMoney(accountFrom.id, accountTo.id, new BigDecimal("4"));

        var balanceFrom = driver.getBalance(accountFrom.id);
        var balanceTo = driver.getBalance(accountTo.id);

        assertEquals(new BigDecimal("1"), balanceFrom.get().balance);
        assertEquals(CurrencyCode.EUR, balanceFrom.get().currencyCode);
        assertEquals(new BigDecimal("12.0"), balanceTo.get().balance);
        assertEquals(CurrencyCode.USD, balanceTo.get().currencyCode);
    }

}
