package org.bitbucket.yanrym.moneytransfer;

import com.mashape.unirest.http.exceptions.UnirestException;
import io.javalin.Javalin;
import org.bitbucket.yanrym.moneytransfer.dto.Account;
import org.bitbucket.yanrym.moneytransfer.testapi.ApiTestDriver;
import org.bitbucket.yanrym.moneytransfer.testapi.SocketUtils;
import org.bitbucket.yanrym.moneytransfer.testapi.TestApp;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ParallelMoneyTransferSimulationTest {

    private Javalin service;
    private ApiTestDriver driver;
    private TestApp.AppWithCtx appWithCtx;

    @BeforeEach
    void setUp() throws Exception {
        service = Javalin.create().start(SocketUtils.getPort());

        appWithCtx = TestApp.createAppWithCtx(service);

        driver = new ApiTestDriver(service);
    }

    @AfterEach
    void tearDown() {
        service.stop();
    }

    @Test
    void canHandleConcurrentTransfers() throws UnirestException, InterruptedException, IOException {
        var sourceAccount = driver.createAccount();
        var originalAmount = new BigDecimal(Long.MAX_VALUE);
        appWithCtx.moneyRepository.addMoneyToAccount(sourceAccount.id, originalAmount);
        var simulationThreads = createSimulationThreads(sourceAccount.id);

        runSimulation(simulationThreads);

        for (var st : simulationThreads) {
            var balance = driver.getBalance(st.account.id);
            assertEquals(st.expectedBalance, balance.get().balance);
        }

        var totalSpent = calculateTotalSpend(simulationThreads);
        var sourceBalance = driver.getBalance(sourceAccount.id);

        assertEquals(originalAmount.subtract(totalSpent), sourceBalance.get().balance);
    }

    private List<SimulationThread> createSimulationThreads(long fromAccount) throws UnirestException {
        var simulationThreads = new ArrayList<SimulationThread>();
        for (int i = 0; i < 10; i++) {
            simulationThreads.add(new SimulationThread(driver, fromAccount, 1000));
        }
        return simulationThreads;
    }

    private void runSimulation(List<SimulationThread> threads) throws InterruptedException {
        threads.forEach(Thread::start);

        for (var simulationThread : threads) {
            simulationThread.join();
        }
    }

    private BigDecimal calculateTotalSpend(List<SimulationThread> threads) {
        return threads.stream().map((st) -> st.expectedBalance).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private static class SimulationThread extends Thread {

        private final Account account;
        private long accountFrom;
        private int iterations;
        private BigDecimal expectedBalance = BigDecimal.ZERO;
        private Random rngGenerator = new Random();
        private ApiTestDriver driver;

        SimulationThread(ApiTestDriver driver, long accountFrom, int iterations) throws UnirestException {
            account = driver.createAccount();
            this.accountFrom = accountFrom;
            this.iterations = iterations;
            this.driver = driver;
        }

        @Override
        public void run() {
            try {
                for (int i = 0; i < iterations; i++) {
                    var amount = new BigDecimal(rngGenerator.nextFloat()).round(MathContext.DECIMAL32);
                    var result = driver.transferMoney(accountFrom, account.id, amount);
                    if (result != -1)
                        System.out.println(result);
                    assertNotEquals(422, result);
                    this.expectedBalance = this.expectedBalance.add(amount);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }


}
