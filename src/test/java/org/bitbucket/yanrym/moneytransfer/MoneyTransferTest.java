package org.bitbucket.yanrym.moneytransfer;

import com.mashape.unirest.http.exceptions.UnirestException;
import io.javalin.Javalin;
import org.bitbucket.yanrym.moneytransfer.exceptions.AccountNotFound;
import org.bitbucket.yanrym.moneytransfer.exceptions.NotEnoughBalance;
import org.bitbucket.yanrym.moneytransfer.testapi.ApiTestDriver;
import org.bitbucket.yanrym.moneytransfer.testapi.SocketUtils;
import org.bitbucket.yanrym.moneytransfer.testapi.TestApp;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MoneyTransferTest {

    private Javalin service;
    private ApiTestDriver driver;
    private TestApp.AppWithCtx appWithCtx;

    @BeforeEach
    void setUp() throws Exception {
        service = Javalin.create().start(SocketUtils.getPort());

        appWithCtx = TestApp.createAppWithCtx(service);

        driver = new ApiTestDriver(service);
    }

    @AfterEach
    void tearDown() {
        service.stop();
    }

    @Test
    void notExistingAccountBalanceIs404() throws UnirestException, IOException {
        var actual = driver.getBalance(123);

        assertTrue(!actual.isPresent(), "Account doesn't exist");
    }

    @Test
    void emptyAccountBalanceIsZero() throws UnirestException, IOException {
        var account = driver.createAccount();
        var actual = driver.getBalance(account.id);

        assertEquals(BigDecimal.ZERO, actual.get().balance);
    }

    @Test
    void canSetBalance() throws UnirestException, IOException {
        var account = driver.createAccount();
        var money = new BigDecimal("154.8345");
        appWithCtx.moneyRepository.addMoneyToAccount(account.id, money);

        var actual = driver.getBalance(account.id);

        assertEquals(money, actual.get().balance);
    }

    @Test
    void canTransferMoneyToOtherAccount() throws UnirestException, IOException {
        var accountFrom = driver.createAccount();
        var accountTo = driver.createAccount();
        var originalAmount = new BigDecimal("123.456");
        var transferAmount = new BigDecimal("72.454");
        appWithCtx.moneyRepository.addMoneyToAccount(accountFrom.id, originalAmount);

        driver.transferMoney(accountFrom.id, accountTo.id, transferAmount);
        var balanceFrom = driver.getBalance(accountFrom.id);
        var balanceTo = driver.getBalance(accountTo.id);

        assertEquals(new BigDecimal("51.002"), balanceFrom.get().balance);
        assertEquals(transferAmount, balanceTo.get().balance);
    }

    @Test
    void canNotTransferMoneyWhenBalanceIsNotEnough() throws UnirestException, IOException {
        var accountFrom = driver.createAccount();
        var accountTo = driver.createAccount();
        var originalAmount = new BigDecimal("10");
        var transferAmount = new BigDecimal("10.01");
        appWithCtx.moneyRepository.addMoneyToAccount(accountFrom.id, originalAmount);

        var error = driver.transferMoney(accountFrom.id, accountTo.id, transferAmount);
        var balanceFrom = driver.getBalance(accountFrom.id);
        var balanceTo = driver.getBalance(accountTo.id);

        assertEquals(NotEnoughBalance.ERROR_CODE, error);
        assertEquals(originalAmount, balanceFrom.get().balance);
        assertEquals(BigDecimal.ZERO, balanceTo.get().balance);
    }

    @Test
    void canNotTransferMoneyWhenSourceAccountDoesntExist() throws UnirestException {
        var accountTo = driver.createAccount();

        var error = driver.transferMoney(TestApp.NOT_EXISTING_ACCOUNT, accountTo.id, BigDecimal.ONE);

        Assertions.assertEquals(AccountNotFound.ERROR_CODE, error);
    }

    @Test
    void canNotTransferMoneyWhenTargetAccountDoesntExist() throws UnirestException {
        var accountFrom = driver.createAccount();

        var error = driver.transferMoney(accountFrom.id, TestApp.NOT_EXISTING_ACCOUNT, BigDecimal.ONE);

        assertEquals(AccountNotFound.ERROR_CODE, error);
    }

}
